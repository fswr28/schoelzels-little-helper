#bin/bash
# @fswr28
# Version: 0.2.0
# Dieses Script entpackt die heruntergeladenen kompletten Abgaben für eine
# Aufgabe von Dozentron in einen Ordner der Wahl. Dabei werden die Jar-Dateien
# in einem Ordner mit dem passenden Namen entpackt.
# Jetzt neu mit entpacker für IO Aufgaben, nie wieder Abgaben übersehen (hoffentlich).
# Für Windows User: Subsystem oder GTFO.

function unpackjar()
{
  ordnername=$(basename $1 | cut -d"." -f1)
  echo $ordnername

  #echo $DIR
  # Dateiname splitten -> nachname_vorname

  unzip  $DIR/$WORKDIR/tmp/$1 -d $DIR/$WORKDIR/$ordnername

}

function unpacktar()
{
  ordnername=$(basename $1 | cut -d"." -f1)
  mkdir $TARGET/$ordnername
  tar -xaf $TARGET/tmp/$1 -C $TARGET/$ordnername


}
#Ordnerpfad holen
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
WORKDIR=$(basename $1 | cut -d"." -f1)
TARGET=$DIR/$WORKDIR

# testen ob zip datei existiert
if [ -f $1 ]
  then
    echo "Datei vorhanden"
  else
    echo "Datei nicht vorhanden."
fi

# testen ob zielverzeichnis existiert
 if [ -d $DIR$WORKDIR ]
  then
    echo "Verzeichnis existiert"
  else
    echo "Verzeichnis existiert nicht, erstelle Verzeichnis"
    mkdir $WORKDIR
fi

# lege tmp verzeichnis an
if [ -d $DIR/$WORKDIR/tmp ]
  then
    echo "tmp Verzeichnis existiert schon"
  else
    mkdir $DIR/$WORKDIR/tmp
fi

# entpacke ZipDatei in tmp Ordner
unzip $1 -d $DIR/$WORKDIR/tmp

#entpacken der jardateien
echo
for file in $DIR/$WORKDIR/tmp/* ; do
  # Wenn Datei .jar:
  #echo $(basename $file)
  # Alternativen für IO Aufgabe:
  # *.input.txt und *.output.txt ignorieren
  # alle anderen dateien: kopieren oder entpacken

	#echo -ne $file "\t"
	case $file in
    #Entpacke Jar Dateien in passenden Ordner
		*.jar) unpackjar $(basename $file) ;;
    #Entpacke Zip Dateien ebenfalls in Ordner
    #und ja ich weiß, ist nicht sauber mit dem Funktionsnamen.
    *.zip) unpackjar $(basename $file) ;;

    # Entferne -input.txt und -output.txt
    # ein bisschen gehacke.....
    *input.txt) echo "Wird ignoriert:  $file" ;;
    *output.txt) echo "Wird ignoriert:  $file" ;;

    # Tarballs entpacken
    *.tar.*) unpacktar $(basename $file)  ;;
    *.tgz) unpacktar $(basename $file)  ;;
		#*.tar.bz2) tar -xjf $(basename $file) -C $TARGET ;;
		#*.tar.bz) echo "tar.bz" ;;
    # Verschiebe alle anderen Files in das verzeichnis
		*) cp $TARGET/tmp/$(basename $file) $TARGET  ;;
	esac
done


rm $DIR/$WORKDIR/tmp -r
