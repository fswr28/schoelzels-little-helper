# SLITHER - Schölzels little Helper



Eine Sammlung von Softwaretools, die die Arbeit mit Dozentron und Moodle einfacher machen sollen.

## MERGE - Moodle Bewertungs Generator

Liest die Daten aus der Bewertungstabelle aus und schreibt sie in eine bereitgestellte Tabelle aus Moodle.

### Vorrausetzungen
- python3 installation

### Usage
- SLITHER.py mit chmod ausführbar machen
- ./SLITHER.py merge -m <Moodle Tabelle> -s <Bewertungs Tabelle>

Zusätzliche Optionen werden mit -h angezeigt.

### Moodle-Tablle
todo: ausfüllen

### Bewertungs Tabelle
todo: ausfüllen

## Todo:
Siehe [https://git.thm.de/fswr28/schoelzels-little-helper/issues](Issues)

## dozentron-jar-entpacker

Entpacker Tool für Abgaben die mit Dozentron https://git.thm.de/dalt40/dozentron eingereicht wurden.
### Vorrausetzungen
- Linux oder LinuxSubsystem
- unzip muss installiert sein

### Usage
- jar_entpacker.sh mit chmod ausführbar machen
- ./jar_entpacker.sh <Archivname\>

Das Archiv wird in den Ordner <Archivname\> entpackt.

### Todo
 + Support für Windows?
 + Support für Leerzeichen im Pfad/Dateiname
Umstellung auf andere Programmiersprache ?


### Known Issues
- Leerzeichen im Pfad oder Ordnernamen lassen Skript abstürzen
