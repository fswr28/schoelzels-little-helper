from merge import Bewertung, Grading_table, Moodle_table


class Merge():
    def __init__(self):
        self.einzel_bewertung = Bewertung.Einzelbewertung()
        self.gesamt_bewertung = Bewertung.Gesamtbewertung()
        self.grading_table = Grading_table.Gradingtable()
        self.moodle_table = Moodle_table.Moodletable()
        self.grade_translator = {'': ['', '']}  # Dict für config
        # self.grade_file = "res/GRADES"
        self.grade_file = "res/GRADES"
        self.grading_task_count = 1
        self.grading_offset = 6

    def read_grading_table(self, filename):
        """
        Reads the CSV Gradingtable and converts it to an Gradingtable Object
        :param filename: The path to the CSV File
        :return:
        """
        self.grading_table.read_csv(filename)

    def read_moodle_table(self, filename):
        """
        Reads the CSV Moodletable and converts it to an Moogletable Object
        :param filename: The path to the CSV File
        :return:
        """
        self.moodle_table.read_csv(filename)

    def read_config(self):
        pass

    def read_grade_translator(self):
        """
        Reads the GRADS config file, for the Translation of the Grades between the table and comment in Moodle
        :return:
        """
        with open(self.grade_file) as grade_file:
            for line in grade_file:
                #print(line)
                # todo: Auf \t ändern?
                split_line = line.split("   ")
                self.grade_translator[split_line[0]] = [split_line[1], split_line[2]]

    # def get_complete_rating(self, rating_function, *grades): # Not needed?

    def set_output_filename(self, filename):
        self.outputfilename = filename

    def set_task_count(self, count):
        self.grading_task_count = count
        self.grading_table.set_task_count(count)

    def set_grading_offset(self, offset):
        self.grading_offset = offset
        self.grading_table.set_offset(offset)

    def merge_grades(self):
        ergebnisse = []
        for student in self.grading_table.content:
            mail_addr = student['E-Mail-Adresse']
            #print(mail_addr)
            #print(student)
            #todo: KeyError Abfangen, nachfragen ob Tabelle richtig Formatiert etc.
            grade = self.grade_translator[student['Ergebnis']][0]
            a = []
            a.append(self.gesamt_bewertung.rate(self.grade_translator[student['Ergebnis']][1]))
            #print(mail_addr)
            for task in range(self.grading_offset + 0, self.grading_offset + (self.grading_task_count * 2), 2):
                task_title = self.grading_table.header[task]
                task_grade = self.grade_translator[student[self.grading_table.header[task]]][1]
                if not task_grade:
                    task_grade = self.grade_translator['b'][1] #todo: richtig machen, andere Bewertung und es macht PENG an der stelle
                task_komment = student[self.grading_table.header[task+1]]
                # a.append(self.einzel_bewertung.rate(self.grading_table.header[task], self.grade_translator[student[self.grading_table.header[task]]][1], student[self.grading_table.header[task+1]]))
                a.append(self.einzel_bewertung.rate(task_title, task_grade, task_komment))
            comment = "".join(a)
            ergebnisse.append([mail_addr, grade, comment])
        return ergebnisse

    def merge_tables(self):
        # Ergebnisse zusammenfassen und sortieren
        ergebnisse = self.merge_grades()
        ergebnisse.sort(key=lambda x: x[0])

        # Ergebnisse in die Moodletabelle reinschreiben:
        for student_a in ergebnisse:
            for student_b in self.moodle_table.content:
                if student_a[0] == student_b['E-Mail-Adresse']:
                    student_b['Bewertung'] = student_a[1]
                    student_b['Feedback als Kommentar'] = student_a[2]

    def save_new_table(self, filename='moodle_output.csv'):
        self.moodle_table.write_csv(filename)





if __name__ == "__main__":
    test = Merge()
    test.read_grade_translator()
    test.read_grading_table("../testdata/Bewertungsheet_test.csv")
    test.read_moodle_table("../testdata/Bewertungen-AuD (SS19)-Testtest- Bitte Ignorieren-237296.csv")
    test.set_grading_offset(6)
    test.set_task_count(6)
    #bla = test.merge_grades()
    test.merge_tables()
    test.save_new_table()
    print("hang on")
