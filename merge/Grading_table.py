import csv


class Gradingtable():
    # todo: Doku neu -FS

    """
    """
    def __init__(self):


        self.task_count = 1
        self.offset = 6
        self.grade_column = 6
        self.header = []
        self.content = []


    def read_csv(self, filename):
        '''
        Reads the csv table given by filename and stores the content of the file in header and content.
        :param filename: filename
        :return:
        '''
        with open(filename) as table_file:
            table = csv.DictReader(table_file)
            self.header = table.fieldnames
            for row in table:
                self.content.append(row)


    def set_task_count(self, count):
        '''
        Sets the count of given tasks to the students
        :param count: count of given tasks
        :return:
        '''
        self.task_count = count

    def set_offset(self, offset):
        '''
        Sets the offset for the csv table, the column where the grades start
        :param offset: new offset
        :return: None
        '''
        self.offset = offset

    def get_column(self, columnname):
        output = []
        if columnname in self.header:
            for row in self.content:
                output.append(row[columnname])
            return output
        else:
            return None

    def get_colums(self, *columnname):
        output= []
        for name in columnname:
            if not name in self.header:
                return None
            else:
                output.append(self.get_column(name))
        return output


    def get_data(self):
        return self.content

    def get_row(self):
        for row in self.content:
            yield row










if __name__ == "__main__":
    f_t = "../testdata/Bewertungsheet_test_auszug.csv"
    test = Gradingtable()
    test.read_csv(f_t)
    print(test.header)
    for ele in test.content:
        print(ele)
    print("ready")


    '''
    for x in range(test.offset, test.offset + test.task_count * 2):
        print(test.fieldnames[x])
    for row in test:
        print(row['Kommentar'])
    #for row in test.reader:
     #   print(row[0])
    '''



