from abc import ABC, abstractmethod


class Bewertung():

    """
    Abstract Class for Bewertungen
    """
    def __init__(self, path):
        self.path = path
        self.text = None
        self.get_template()

    def get_template(self):
        """
        Reads the template for the Bewertung
        :return:
        """
        res = []
        with open(self.path) as file:
            for line in file:
                res.append(line)
        self.text = "".join(res)


    @abstractmethod
    def rate(self):
        pass


class Gesamtbewertung(Bewertung):
    def __init__(self):
        super().__init__("res/vorlage_gesamtbewertung")

    def rate(self, note):
        """
        Returns the formatted string for gesamtbewertung
        :param grade: the Grade for gesamtbewertung
        :return: String
        """
        return self.text.format(note)


class Einzelbewertung(Bewertung):

    def __init__(self):
        super().__init__("res/vorlage_einzelaufgabe")

    def rate(self,  taskname, grade, comment):
        """
        Returns the formatted string for einzelaufgabe
        The grade
        :param taskname: The name of the task
        :param grade: The grade of the task
        :param comment: comment for the student
        :return: String
        """
        return self.text.format(taskname, grade, comment)


