import csv


class Moodletable():

    """
    Klasse for Moodle csv Table
    """

    def __init__(self):
        self.task_count = 1
        self.offset = 6
        self.header = []
        self.content = []
        self.filename = None

    def read_csv(self, filename):
        '''
        Reads the csv table given by filename and stores the content of the file in header and content.
        :param filename: filename
        :return:
        '''
        self.filename = filename
        with open(filename) as table_file:
            table = csv.DictReader(table_file)
            self.header = table.fieldnames
            for row in table:
                self.content.append(row)

    def write_csv(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, 'w') as csv_file:
            writer = csv.DictWriter(csv_file, self.header)
            writer.writeheader()
            writer.writerows(self.content)


    def get_row(self, id_type, id):
        '''

        :param id_type:
        :param id:
        :return:
        '''
        if id_type in self.header:
            for row in self.content:
                if row[id_type] == id:
                    return row
        return None

    def change_row(self, row, id_type="email"):
        if id_type is 'email':
            id_type = 'E-Mail-Adresse'
        for contend_row in self.content:
            if contend_row[id_type] == row[id_type]:
                contend_row = row










if __name__ == "__main__":
    test = Moodletable()
    test.read_csv("../testdata/Bewertungen-AuD (SS19)-Testtest- Bitte Ignorieren-237296.csv")
    test.write_csv("../testdata/test.csv")
    email = 'E-Mail-Adresse'

