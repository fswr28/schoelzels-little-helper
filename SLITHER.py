#!/usr/bin/env python3

import argparse
import sys


class SLITHER():
    def __init__(self):
        """
        Code for Multi-level argpase taken from https://chase-seibert.github.io/blog/2014/03/21/python-multilevel-argparse.html#
        For other developer:
        You just need to add another funktion with the name of the subtool you addet to the SLITHER Toolbox with
        all the needed arguments.
        """
        descripttion = ""
        self.parser = argparse.ArgumentParser(description='''\
            ### SLITHER - Schoelzels little helper ###
        A toolcollection to help with Dozentron and Moodle
        MERGE: Moodle Bewertungs Generator= merge
        xxx: Dozentron Jar xxx = xxx
        CRAVER: Commissural Markdown Converter= carver
        ''',
                                              usage= "./slither.py <tool> [options]", formatter_class=argparse.RawDescriptionHelpFormatter)
        self.parser.add_argument("tool", help="Specifies the tool to use")
        self.parser.add_argument("--version, -v", action='version', version='%(progs)s 0.2')
        args = self.parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.tool):
            self.parser.print_help()
            exit(1)
        getattr(self, args.tool)()

    """
    """
    def merge(self):

        from merge.MERGE import Merge
        print("Ready to MERGE")

        self.parser = argparse.ArgumentParser(description="Merges two .csv files, a Grading Sheet and a Moodle Sheet for im- and exporting grades\n"
                                                          "to one filled out Moodle Sheet with grades and comments")
        self.parser.add_argument("--version, -v", action='version', version='%(progs)s 0.2')
        self.parser.add_argument("-m", "--moodle-input", help="The moodle .csv File, downloaded from the course", required=True)
        self.parser.add_argument("-s", "--score_input", help="The CSV Table with the scores for the assesment.", required=True)
        self.parser.add_argument("-o", "--output", help="Specifies the name of the outputfile", default="output_moodle.csv")
        # self.parser.add_argument("--without_comments", help="Generates the outputfile without the given comments")
        self.parser.add_argument("-c", "--count_tasks",type=int, help="Sets the amount of tasks given to the students", default=1)
        self.parser.add_argument("--offset", type=int, help="Sets the offset of the grading .csv table", default=6)
        args = self.parser.parse_args(sys.argv[2:])

        # Create MERGE Object and merge Tables
        merger = Merge()
        merger.read_grade_translator()
        merger.read_grading_table(args.score_input)
        merger.read_moodle_table(args.moodle_input)
        merger.set_grading_offset(args.offset)
        merger.set_task_count(args.count_tasks)
        # bla = test.merge_grades()
        merger.merge_tables()
        merger.save_new_table(args.output)
        print("Done MERGing")


if __name__ == "__main__":
    SLITHER()

