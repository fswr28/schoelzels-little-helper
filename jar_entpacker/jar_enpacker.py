import zipfile
import tarfile


class Jar_Entpacker():
    def __init__(self):
        pass

    def extract_zip(self, file):
        with zipfile.ZipFile(file, 'r') as jar_file:
            jar_file.extractall(self.get_foldername(jar_file))

    def extract_jar(self, file):
        self.extract_zip(file)

    def extract_tar(self, file):
        with tarfile.TarFile(file, 'r:*') as tar_file:
            tar_file.extractall(self.get_foldername(file))

    def get_foldername(self, filename):
        '''
        Extracts the name for the folder from the Jar/Zip File
        :param filename:
        :return:
        '''

        # fummel am Dateinamen rum und entferne unötigen kram (nur bei dozentron abgaben)
        #  was ist mit pfadangaben?
        return filename.split(".")[0]

    def unpack(self):
        pass







# with zipfile.ZipFile("file.zip","r") as zip_ref:
#    zip_ref.extractall("targetdir")